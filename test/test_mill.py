import timeit

def test_cached_mill():
  from pythiamill import CachedPythiaMill
  from pythiamill.pythia import Counter
  from pythiamill.config import test_pythia_options

  batch_size = 32
  n_batches = 32

  detector = Counter(22)
  mill = CachedPythiaMill(
    detector, test_pythia_options,
    batch_size=batch_size, cache_size=32,
    n_workers=4, seed=123
  )

  time = timeit.timeit(
    lambda: mill.sample(),
    number=n_batches
  )
  mill.shutdown()

  print('%.2lf iters/sec' % (1 / time, ))

def test_free_mill():
  from pythiamill import FreePythiaMill
  from pythiamill.config import test_pythia_options

  mill = FreePythiaMill(test_pythia_options, cache_size=32, n_workers=4, seed=123)

  for i in range(100):
    _ = mill.sample()

  mill.shutdown()

def test_parametrized_mill():
  from pythiamill import ParametrizedPythiaMill
  from pythiamill.pythia import Counter
  from pythiamill.config import test_pythia_options

  detector = Counter(22)
  mill = ParametrizedPythiaMill(detector, test_pythia_options, n_workers=4, seed=123)

  for i in range(10):
    mill.request(i / 10.0)

  for i in range(10):
    _ = mill.retrieve()

  mill.shutdown()