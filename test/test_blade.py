import numpy as np
import multiprocessing as mp

N = 10

def test_free_blade():
  from pythiamill.mill import free_pythia_blade
  from pythiamill.config import test_pythia_options

  ctx = mp.get_context('spawn')

  command_queue = ctx.Queue()
  result_queue = ctx.Queue()

  for _ in range(N):
    command_queue.put(())
  command_queue.put(None)

  _ = free_pythia_blade(command_queue, result_queue, options=test_pythia_options)

  for i in range(N):
    _, result = result_queue.get(block=True, timeout=1)
    print(i, len(result))

  print(result_queue.get(block=True))

def test_blade():
  from pythiamill.mill import detector_pythia_blade
  from pythiamill.pythia import Counter
  from pythiamill.config import test_pythia_options

  ctx = mp.get_context('spawn')

  command_queue = ctx.Queue()
  result_queue = ctx.Queue()
  detector = Counter()

  for _ in range(N):
    command_queue.put(())
  command_queue.put(None)

  _ = detector_pythia_blade(
    detector,
    command_queue, result_queue,
    options=test_pythia_options, batch_size=32
  )

  for i in range(N):
    _, arr = result_queue.get(block=True, timeout=5)
    print(i, np.sum(arr))

  print(result_queue.get(block=True))

def test_blade_process():
  from pythiamill.mill import detector_pythia_blade
  from pythiamill.pythia import Counter
  from pythiamill.config import test_pythia_options

  ctx = mp.get_context('spawn')

  command_queue = ctx.JoinableQueue()
  result_queue = ctx.JoinableQueue()
  detector = Counter(10)

  p = ctx.Process(
    target=detector_pythia_blade,
    kwargs=dict(
      detector_factory=detector,
      command_queue=command_queue,
      queue=result_queue,
      options=test_pythia_options,
      batch_size=32
    )
  )

  p.start()

  command_queue.put(())
  command_queue.put(None)

  print('Is process alive:', p.is_alive())

  print(result_queue.get(block=True))
  print(result_queue.get(block=True))

  p.terminate()

