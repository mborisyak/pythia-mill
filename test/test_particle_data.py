def test_particle_data():
  import pythiamill as pm
  from pythiamill.config import test_pythia_options

  pythia = pm.pythia.launch_pythia(test_pythia_options)
  data = pythia.get_particle_data()
  for k, v in data.items():
    print(k, v)