import pythiamill as pm
import timeit

def main(n_samples=1024, repeat=10):
  mill = pm.FreePythiaMill(
    options=pm.config.test_pythia_options,
    cache_size=32,
    n_workers=None,
    seed=1234
  )

  time = timeit.timeit(lambda: mill.sample(n_samples), number=repeat)
  print('%.2lf iter/sec' % (n_samples * repeat / time,))

  event, = mill.sample(1)

  for particle in event[:10]:
    print(particle)

if __name__ == '__main__':
  main()