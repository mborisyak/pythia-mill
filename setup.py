"""
  Pythia Mill
"""

from setuptools import setup, find_packages, Extension

from codecs import open
import os
import numpy as np


import ensurepythia
shared_location, include_location, share_location = ensurepythia.ensure_pythia()
print('Using %s and %s to build against Pythia.' % (shared_location, include_location))

def split_libs(var):
  return [
    path for path in var.split(':') if len(path) > 0
  ]

def get_includes():
  env = os.environ

  includes = []

  for k in ['CPATH', 'C_INCLUDE_PATH', 'INCLUDE_PATH']:
    if k in env:
      includes.extend(split_libs(env[k]))

  return includes + [include_location]

def get_library_dirs():
  env = os.environ

  libs = []

  for k in ['LD_LIBRARY_PATH']:
    if k in env:
      libs.extend(split_libs(env[k]))

  return libs + [shared_location]

from Cython.Build import cythonize

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
  long_description = f.read()

extra_compile_args=ensurepythia.compilation_options() + ['-g']
extra_link_args=['-g']
print('Compiling with: %s' % (' '.join(extra_compile_args), ))

def get_source_files(target: str):
  path = target.split('.')
  dirname, filename = path[:-1], path[-1]

  root = os.path.join('src', *dirname)
  source = os.path.join(root, '%s.pyx' % (filename,))

  return [source]

def extension(target: str, *dependencies):
  return Extension(
    target,
    get_source_files(target) + list(dependencies),
    libraries=['stdc++', 'pythia8'],
    include_dirs=[np.get_include()] + get_includes(),
    library_dirs=get_library_dirs(),
    language='c++',
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
  )

extensions = [
  extension('pythiamill.pythia.pythiautils'),

  extension('pythiamill.pythia.detector'),
  extension('pythiamill.pythia.counter'),
  extension('pythiamill.pythia.pid'),
  extension('pythiamill.pythia.tunemcdetector', 'src/pythiamill/pythia/TuneMC.cpp'),
  extension('pythiamill.pythia.stracker'),
]

setup(
  name='pythiamill',

  version='1.2.0',

  description="""Pythia generator for python.""",

  long_description = long_description,

  url='https://gitlab.com/mborisyak/pythia-mill',

  author='Maxim Borisyak',
  author_email='maximus.been at gmail dot com',

  maintainer = 'Maxim Borisyak',
  maintainer_email = 'maximus.been at gmail dot com',

  license='MIT',

  classifiers=[
    'Development Status :: 4 - Beta',
    'Intended Audience :: Science/Research',
    'Topic :: Scientific/Engineering :: Physics',
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3',
  ],

  keywords='Pythia',

  packages=find_packages('src'),
  package_dir={'': 'src/'},

  extras_require={
    'dev': ['check-manifest'],
    'test': ['nose>=1.3.0'],
  },

  install_requires=[
    'numpy',
    'cython',
    'ensurepythia == 8.3.03'
  ],

  ext_modules = cythonize(
    extensions,
    gdb_debug=True,
    compiler_directives={
      'embedsignature': True,
      'language_level' : 3
    }
  ),
)
