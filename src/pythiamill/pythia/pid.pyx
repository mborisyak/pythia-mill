from .pythiautils cimport Pythia, FLOAT
from .detector cimport Detector

cimport numpy as cnp

ctypedef cnp.uint8_t uint8

class PIDWrapper(object):
  """
  For pickle.
  """
  def __init__(self, size):
    self.args = (size, )

  def __call__(self):
    return PID(*self.args)

  def event_size(self,):
    return self.size

cdef class PID(Detector):
  def __init__(self, int size):
    self.size = size

  def event_size(self):
    return self.size

  cpdef void view(self, FLOAT[:] buffer, tuple args):
    cdef Pythia * pythia = self.pythia

    cdef int i, event_size, code

    event_size = pythia.event.size()
    if event_size > self.size:
      event_size = self.size

    for i in range(event_size):
      code = pythia.event.at(i).id()
      buffer[i] = code
