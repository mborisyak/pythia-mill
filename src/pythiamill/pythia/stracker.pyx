import cython
from libc.stdlib cimport rand, RAND_MAX
from libc.math cimport sqrt, log, exp, atanh, tanh, atan2, M_PI, floor

from libcpp.unordered_set cimport unordered_set

cimport numpy as cnp
import numpy as np

from .pythiautils cimport Pythia, FLOAT
from .detector cimport Detector

ctypedef cnp.uint8_t uint8

cdef inline double abs(double x) nogil:
  return -x if x < 0 else x

cdef inline double uniform() nogil:
  return (<double>rand()) / RAND_MAX

cdef inline double intersection_scale(
  double o, double ox, double oy, double oz,
  double p, double px, double py, double pz,
  double R_sqr
) nogil:
  """
  Solves
    (ox + scale * px) ** 2 + (oy + scale * py) ** 2 + (oz + scale * pz) ** 2 = R ** 2
  for scale.
  """
  cdef double d
  cdef double scalar_prod

  scalar_prod = px * ox + py * oy + pz * oz

  d = 4 * scalar_prod * scalar_prod +  4 * p * (R_sqr - o)
  if d < 0.0:
    return -1.0
  else:
    return (0.5 * sqrt(d) - scalar_prod) / p

cdef inline double leakage_normalization(int radius, double leakage_coefficient):
  cdef double Z = 0.0

  cdef int i, j
  cdef int r_sqr

  cdef double R_sqr = radius * radius

  for i in range(-radius, radius + 1):
    for j in range(-radius, radius + 1):
      r_sqr = i * i + j * j

      if r_sqr > R_sqr:
        continue

      Z += exp(-leakage_coefficient * r_sqr)

  return Z

cdef double LOG_2 = log(2.0);

class SphericalTrackerWrapper(object):
  """
  For pickle.
  """
  def __init__(
    self,
    pseudorapidity_steps=32, phi_steps=32, max_pseudorapidity=5,
    layers_radiuses=1,
    ### photon
    fully_interacting_particles=(22, ),
    ### neutrinos, Kaons
    non_interacting_particles=(-12, -14, -16, 12, 14, 16, 130, 310),
    energy_loss=0.01, leakage_radius=2, leakage_coefficient=LOG_2
  ):
    """
    A python wrapper for the spherical tracker detector which consists of multiple spheres (layers)
    with "holes": R = const, phi <- [-pi, pi], pseudo rapidity <- [0, `max_pseudorapidity`].

    Each sphere is covered with a pixel grid uniformly distributed in the phi-pseudorapidity space - each pixel records
    a portion of energy (`energy_loss` parameter) of particles passing through the pixel, with the following exceptions:
    - particles from `fully_interacting_particles` release all their energy in the first layer, and do not interact with other layers;
    - particles from `non_interacting_particles` are simply ignored (by default, the list includes all neutrinos, K-short and K-long).

    Additionally, energy is redistributed to the nearby pixels within `leakage_radius`:
    `e_ij = original_energy / Z * exp(-leakage_coefficient * r)`
    where:
    - `r` - l2 distance (in pixels) between the incident pixel and a pixel with coordinates (i, j);
    - `Z` - normalization constant.

    Each sphere has an offset relative to the collision point.

    :param pseudorapidity_steps: number of pixels along the pseudo-rapidity axis;
    :param phi_steps: number of pixels along the phi-axis, where phi is the angle in the plane orthogonal to the beam (z-axis);
    :param max_pseudorapidity: maximal pseudo-rapidity the spheres cover;
    :param layers_radiuses: an array with radiuses of the spheres, also this parameter is used to determine the number of layers;
    :param fully_interacting_particles: a list of particle ids that interact fully (release all energy) but only with the first layer;
    :param non_interacting_particles: a list of particle ids that do not interact with the detector at all;
    :param energy_loss: the portion of particle energy that is deposited into each pixel that the particle intersects;
    :param leakage_radius: radius (in pixels) of energy redistribution (leakage);
    :param leakage_coefficient: the coefficient of energy redistribution: 0 leads to the uniform energy redistribution (within the `leakage_radius`),
      +inf effectively means no redistribution (however, to achieve this, it is better to set `leakage_radius` to 0).
    """
    if layers_radiuses is None:
      layers_radiuses = 1

    if isinstance(layers_radiuses, int):
      self.layers_radiuses = np.linspace(0, 1, num=layers_radiuses + 1, dtype='float64')[1:]
    else:
      self.layers_radiuses = layers_radiuses

    self.pseudorapidity_steps = pseudorapidity_steps
    self.phi_steps = phi_steps
    self.max_pseudorapidity = max_pseudorapidity

    self.fully_interacting_particles = fully_interacting_particles
    self.non_interacting_particles = non_interacting_particles

    self.energy_loss = energy_loss
    self.leakage_radius = leakage_radius
    self.leakage_coefficient = leakage_coefficient

  def __call__(self):
    return SphericalTracker(
      self.pseudorapidity_steps, self.phi_steps, self.max_pseudorapidity,
      self.layers_radiuses,

      self.fully_interacting_particles,
      self.non_interacting_particles,

      self.energy_loss, self.leakage_radius, self.leakage_coefficient
    )

  def event_size(self):
    return self.layers_radiuses.shape[0] * self.pseudorapidity_steps * self.phi_steps

cdef class SphericalTracker(Detector):
  def __cinit__(self):
    self.non_interacting = unordered_set[int]()
    self.fully_interacting = unordered_set[int]()

  def __init__(self,
    int pseudorapidity_steps, int phi_steps, double max_pseudorapidity,
    double[:] layers_radiuses,
    ### photon
    tuple fully_interacting_particles,
    ### neutrinos, Kaons
    tuple non_interacting_particles,
    double energy_loss, int leakage_radius, double leakage_coefficient
  ):
    self.pr_steps = pseudorapidity_steps
    self.phi_steps = phi_steps
    self.max_pseudorapidity = max_pseudorapidity

    self.energy_loss = energy_loss

    self.leakage_radius = leakage_radius
    self.leakage_coefficient = leakage_coefficient
    self.leakage_Z = leakage_normalization(leakage_radius, leakage_coefficient)

    self.layers_R = layers_radiuses
    self.layers_Rsqr = np.ndarray(shape=(self.layers_R.shape[0], ), dtype='float64')

    cdef int i
    for i in range(self.layers_R.shape[0]):
      self.layers_Rsqr[i] = self.layers_R[i] * self.layers_R[i]


    for i in fully_interacting_particles:
      self.fully_interacting.insert(i)

    for i in non_interacting_particles:
      self.non_interacting.insert(i)

  def event_size(self):
    return self.layers_R.shape[0] * self.pr_steps * self.phi_steps

  @cython.boundscheck(False)
  @cython.overflowcheck(False)
  cpdef void view(self, FLOAT[:] buffer, tuple args):
    cdef double offset_x, offset_y, offset_z
    cdef float64[:] offsets

    cdef int i, j

    if len(args) > 0:
      offsets = np.array(args[0], dtype='float64')
      assert offsets.shape[0] == 3 * self.layers_R.shape[0], \
        'Spherical detector accepts either no arguments or 3 * <number of layers> offsets, got %d' % (offsets.shape[0], )
    else:
      offsets = None

    cdef Pythia * pythia = self.pythia

    ### ...
    cdef double max_pseudorapidity = self.max_pseudorapidity

    ### utility constant, maximal p_z / ||p||
    cdef double max_tan = tanh(max_pseudorapidity)

    ### number of steps in the pseudorapidity axis
    cdef int pr_steps = self.pr_steps
    ### size of one pseudorapidity step
    cdef double pr_step = 2 * max_pseudorapidity / pr_steps

    ### the same for phi
    cdef int phi_steps = self.phi_steps
    cdef double phi_step = 2 * M_PI / phi_steps

    ### momentum
    cdef double px, py, pz

    ### origin coordinates
    cdef double ox, oy, oz

    ### decay (end) coordinates
    cdef double dx, dy, dz

    ### utility deltas
    cdef double ax, ay, az

    ### coordinates of intersection with the pythia sphere
    cdef double ix, iy, iz

    ### pseudorapidity, the other angle
    cdef double pr, phi

    ### norm of the origin, decay, momentum and delta vectors, squared
    cdef double o, d, p, a

    ### squared radius of the current layer
    cdef double R, R_sqr

    ### || o + scale * p || = R
    cdef double scale

    ### tanh of pseudorapidity
    ### pr = atanh(iz / R), thus th = iz / R
    cdef double th

    ### position of the cells in the grid
    cdef int pr_i, phi_i

    ### component of the momentum traverse to the pixel
    cdef double pt

    cdef int indx
    cdef int pid

    buffer[:] = 0.0

    for i in range(pythia.event.size()):
      pid = pythia.event.at(i).id()
      ### skip non-interacting particles
      if self.non_interacting.count(pid) > 0:
        continue

      for j in range(self.layers_R.shape[0]):
        ### skip fully-interacting particles if it is not the first layer
        if self.fully_interacting.count(pid) > 0 and j > 0:
          continue

        if offsets is not None:
          offset_x = offsets[3 * j]
          offset_y = offsets[3 * j + 1]
          offset_z = offsets[3 * j + 2]
        else:
          offset_x = 0.0
          offset_y = 0.0
          offset_z = 0.0

        R_sqr = self.layers_Rsqr[j]
        R = self.layers_R[j]

        px = pythia.event.at(i).px()
        py = pythia.event.at(i).py()
        pz = pythia.event.at(i).pz()

        p = px * px + py * py + pz * pz

        if p < 1.0e-12:
          ### I guess, nobody would miss such particles
          continue

        ox = pythia.event.at(i).xProd() + offset_x
        oy = pythia.event.at(i).yProd() + offset_y
        oz = pythia.event.at(i).zProd() + offset_z

        dx = pythia.event.at(i).xDec() + offset_x
        dy = pythia.event.at(i).yDec() + offset_y
        dz = pythia.event.at(i).zDec() + offset_z

        ax = dx - ox
        ay = dy - oy
        az = dz - oz
        a = ax * ax + ay * ay + az * az

        if a < 1.0e-9:
          ### particle decayed immediately
          continue

        o = ox * ox + oy * oy + oz * oz
        d = dx * dx + dy * dy + dz * dz

        if (o >= R_sqr and d >= R_sqr) or (o <= R_sqr and d <= R_sqr):
          ### the particle originates and decays
          ### either outside or inside the pythia
          continue

        ### solution of ||origin + scale * (decay - origin)|| = R for scale
        scale = intersection_scale(o, ox, oy, oz, a, ax, ay, az, R_sqr)

        if scale < 0.0 or scale > 1.0:
          ### this should not happen
          continue

        ### coordinates of intersection
        ix = ox + scale * ax
        iy = oy + scale * ay
        iz = oz + scale * az

        pt = px * ix + py * iy + pz * iz
        pt /= sqrt(ix * ix + iy * iy + iz * iz)

        ### ix ** 2 + iy ** 2 + iz ** 2 must sum to R ** 2
        th = abs(iz) / R

        ### to avoid expensive atanh call
        ### Note: tanh and atanh are monotonous.
        if th >= max_tan:
          ### particle too close to the beam axis
          continue

        ### actual pseudorapidity (abs of it)
        pr = atanh(th)
        pr_i = <int> floor(pr / pr_step)

        ### the negative semi-sphere.
        if iz < 0:
          pr_i = -pr_i - 1

        pr_i += pr_steps // 2

        ### phi is just atan, pi shift is just to compensate for negative angels
        phi = atan2(iy, ix) + M_PI
        phi_i = <int> floor(phi / phi_step)

        # ### tracker activation
        # if pythia.event.at(i).isCharged():
        #   indx = j * (phi_steps * pr_steps) + pr_i * phi_steps + phi_i
        #
        #   if self.is_binary:
        #     buffer[indx] = 1.0
        #   else:
        #     buffer[indx] += pt
        #
        # if pythia.event.at(i).id() == 22 and self.photon_detection > 0:
        #   if self.is_binary:
        #     buffer[indx] = 1.0
        #   else:
        #     buffer[indx] += p
        #
        #   break
