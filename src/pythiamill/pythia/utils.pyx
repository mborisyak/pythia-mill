cimport cython
import cython
from .pythiautils cimport Pythia, Event, FLOAT
from .detector cimport Detector

cimport numpy as cnp
import numpy as np

from libc.math cimport sqrt, atanh, tanh, atan2, M_PI, floor
from libc.stdlib cimport rand, srand, RAND_MAX

# cdef inline double abs(double x) nogil
# cdef inline double uniform() nogil