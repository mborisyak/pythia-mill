from .pythiautils cimport Pythia, FLOAT

cdef class Detector:
  cdef bind(self, Pythia * pythia):
    self.pythia = pythia

  cpdef void view(self, FLOAT[:] buffer, tuple args):
    pass

  def event_size(self):
    return 0