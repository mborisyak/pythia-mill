from libcpp.utility cimport pair
from cython.operator cimport dereference as deref, preincrement as inc
from libcpp.string cimport string

from .pythiautils cimport Pythia, Particle, PyParticle
from .pythiautils cimport FLOAT
from .pythiautils cimport ParticleData, ParticleDataEntry, PyParticleEntry
from .detector cimport Detector

from libc.stdlib cimport malloc
ctypedef cnp.uint8_t uint8

cdef char * _chars(bytes s):
  cdef char * str
  cdef int i

  str = <char *>malloc((len(s) + 1) * sizeof(char))

  for i in range(len(s)):
    str[i] = s[i]

  str[len(s)] = b'\0'
  return str

cdef class PyParticleEntry:
  def __init__(self, id, name, spin_type, charge_type, col_type, m0, tau0, has_anti, is_resonance):
    self.id = id
    self.name = name
    self.spin_type = spin_type
    self.charge_type = charge_type
    self.col_type = col_type
    self.m0 = m0
    self.tau0 = tau0
    self.has_anti = has_anti
    self.is_resonance = is_resonance

  def __str__(self):
    return '%s [%d] (spin=%d, charge=%d, m0=%.3e)' % (
      self.name, self.id, self.spin_type, self.charge_type, self.m0
    )

  def __repr__(self):
    return str(self)

cdef PyParticleEntry pyparticle_from_particle(ParticleDataEntry entry):
  return PyParticleEntry(
    id=entry.id(),
    name=entry.name(),
    spin_type=entry.spinType(),
    charge_type=entry.chargeType(),
    col_type=entry.colType(),
    m0=entry.m0(),
    tau0=entry.tau0(),
    has_anti=entry.hasAnti(),
    is_resonance=entry.isResonance()
  )

cdef class PyPythia:
  def __init__(self):
    cdef bytes xmlDir
    try:
      import ensurepythia
      import os
      _, _, share_location = ensurepythia.locate_pythia()
      xmlDir = bytes(os.path.join(share_location, 'Pythia8/xmldoc'), encoding='utf-8')
    except:
      xmlDir = b'../share/Pythia8/xmldoc'

    self.pythia = new Pythia(string(_chars(xmlDir)), False)

  cdef Pythia * get_pythia(self) nogil:
    return self.pythia

  cdef ParticleData * particle_data(self) nogil:
    return &self.pythia.particleData

  def get_particle_data(self):
    data = dict()

    cdef map[int, ParticleDataEntry].iterator it = self.pythia.particleData.begin()
    cdef pair[int, ParticleDataEntry] entry

    while it != self.pythia.particleData.end():
      entry = deref(it)
      data[entry.first] = pyparticle_from_particle(entry.second)
      inc(it)

    return data

  def __dealloc__(self):
    del self.pythia

cpdef void configure_pythia(PyPythia pypythia, list options):
  cdef int opt_len = len(options)
  cdef string cpp_str
  cdef int i, j
  cdef bytes py_str

  for i in range(opt_len):
    cpp_str = _chars(bytes(options[i], encoding='utf-8'))
    pypythia.get_pythia().readString(cpp_str)
  pypythia.get_pythia().init()


cpdef PyPythia launch_pythia(list options):
  cdef PyPythia pypythia = PyPythia()
  configure_pythia(pypythia, options)

  return pypythia

# @cython.boundscheck(False)
# @cython.wraparound(False)
cpdef void detector_pythia_worker(Detector detector, PyPythia pypythia, FLOAT[:, :] buffer, tuple args):
  cdef Pythia * pythia = pypythia.get_pythia()
  detector.bind(pythia)

  cdef int i = 0
  while i < buffer.shape[0]:
    if not pythia.next():
      continue

    detector.view(buffer[i], args)
    i += 1

cdef class PyParticle:
  def __init__(
    self, id, status,
    mother1, mother2, daughter1, daughter2,
    px, py, pz, e,
    xProd, yProd, zProd, tProd,
    xDec, yDec, zDec, tDec,
    tau
  ):
    self.id = id
    self.status = status
    self.mother1 = mother1
    self.mother2 = mother2
    self.daughter1 = daughter1
    self.daughter2 = daughter2

    self.px = px
    self.py = py
    self.pz = pz
    self.e = e

    self.xProd = xProd
    self.yProd = yProd
    self.zProd = zProd
    self.tProd = tProd

    self.xDec = xDec
    self.yDec = yDec
    self.zDec = zDec
    self.tDec = tDec

    self.tau = tau

  def __str__(self):
    return 'Particle(id=%d, status=%d, ' \
           'origin=(%.2e, %.2e, %.2e, %.2e), ' \
           'decay=(%.2e, %.2e, %.2e, %.2e), ' \
            'p=(%.2e, %.2e, %.2e, %.2e), ' \
           'tau=%.2e, final=%s)' % (
      self.id,
      self.status,
      self.xProd, self.yProd, self.zProd, self.tProd,
      self.xDec, self.yDec, self.zDec, self.tDec,
      self.px, self.py, self.pz, self.e,
      self.tau, self.isFinal()
    )

  def __repr__(self):
    return str(self)

  cpdef bool isFinal(self):
    return self.status > 0

cdef PyParticle from_pythia_particle(Particle& particle):
  return PyParticle(
    id=particle.id(),
    status=particle.status(),
    mother1=particle.mother1(),
    mother2=particle.mother2(),
    daughter1=particle.daughter1(),
    daughter2=particle.daughter2(),
    px=particle.px(),
    py=particle.py(),
    pz=particle.pz(),
    e=particle.e(),

    xProd=particle.xProd(),
    yProd=particle.yProd(),
    zProd=particle.zProd(),
    tProd=particle.tProd(),

    xDec=particle.xDec(),
    yDec=particle.yDec(),
    zDec=particle.zDec(),
    tDec=particle.tDec(),

    tau=particle.tau(),
  )

cpdef list free_pythia_worker(PyPythia pypythia):
  cdef Pythia * pythia = pypythia.get_pythia()

  cdef list result = list()

  cdef int i = 0
  while not pythia.next():
    continue

  for i in range(pythia.event.size()):
    result.append(from_pythia_particle(pythia.event.at(i)))

  return result