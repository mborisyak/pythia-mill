from libcpp.unordered_set cimport unordered_set

from .pythiautils cimport float64, FLOAT
from .detector cimport Detector

cdef class SphericalTracker(Detector):
  cdef int pr_steps
  cdef int phi_steps
  cdef double max_pseudorapidity

  cdef double energy_loss

  cdef int leakage_radius
  cdef double leakage_coefficient
  cdef double leakage_Z

  cdef double[:] layers_R
  cdef double[:] layers_Rsqr

  cdef unordered_set[int] fully_interacting
  cdef unordered_set[int] non_interacting

  cpdef void view(self, FLOAT[:] buffer, tuple args)
