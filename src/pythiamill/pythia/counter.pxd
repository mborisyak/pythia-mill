from .pythiautils cimport FLOAT
from .detector cimport Detector

cdef class Counter(Detector):
  cdef list particles_to_count
  cdef dict particle_index

  cpdef void view(self, FLOAT[:] buffer, tuple args)
