from .pythiautils cimport FLOAT
from .detector cimport Detector

cdef class PID(Detector):
  cdef int size

  cpdef void view(self, FLOAT[:] buffer, tuple args)
