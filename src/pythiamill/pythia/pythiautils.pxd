from libcpp cimport bool
from libcpp.map cimport map
from libcpp.string cimport string

cdef extern from "Pythia8/Event.h" namespace "Pythia8" nogil:
  cdef cppclass Particle:
    int    id()        nogil
    int    status()    nogil
    int    mother1()   nogil
    int    mother2()   nogil
    int    daughter1() nogil
    int    daughter2() nogil
    int    col()       nogil
    int    acol()      nogil
    double px()        nogil
    double py()        nogil
    double pz()        nogil
    double e()         nogil
    double m()         nogil
    double scale()     nogil
    double pol()       nogil
    bool   hasVertex() nogil
    double xProd()     nogil
    double yProd()     nogil
    double zProd()     nogil
    double tProd()     nogil

    double xDec()      nogil
    double yDec()      nogil
    double zDec()      nogil
    double tDec()      nogil

    double tau()       nogil

    int    idAbs()     nogil
    int    statusAbs() nogil
    bool   isFinal()   nogil
    int    intPol()    nogil

    bool isCharged() nogil

  cdef cppclass Event:
    Particle& front() nogil
    Particle& at(int i) nogil
    Particle& back() nogil

    int size() nogil;

cdef extern from "Pythia8/ParticleData.h" namespace "Pythia8" nogil:
  cdef cppclass ParticleDataEntry:
    int id() nogil const
    int antiId() nogil const
    bool hasAnti() nogil const
    string name(int idIn = 1) nogil const
    int spinType() nogil const
    int chargeType(int idIn = 1) nogil const
    double charge(int idIn = 1) nogil const
    int colType(int idIn = 1) nogil const
    double m0() nogil const
    double tau0() nogil const
    bool isResonance() nogil const
    bool doExternalDecay() nogil const
    bool isVisible() nogil const
    bool canDecay() nogil const
    bool isLepton() nogil const
    bool isQuark() nogil const
    bool isGluon() nogil const
    bool isDiquark() nogil const
    bool isParton() nogil const
    bool isHadron() nogil const
    bool isMeson() nogil const
    bool isBaryon() nogil const
    bool isOnium() nogil const

  cdef cppclass ParticleData:
    bool isParticle(int idIn) nogil const
    ParticleDataEntry* findParticle(int idIn) nogil
    int nextId(int idIn) nogil const
    void checkTable(int verbosity = 1)
    void listAll()

    map[int, ParticleDataEntry].iterator begin()
    map[int, ParticleDataEntry].iterator end()

cdef class PyParticleEntry:
  cpdef int id
  cpdef bytes name
  cpdef int spin_type, charge_type, col_type
  cpdef double m0, tau0
  cpdef bool has_anti, is_resonance

cdef extern from "Pythia8/Pythia.h" namespace "Pythia8" nogil:
  cdef cppclass Pythia:
    Pythia(string xmlDir, bool printBanner) nogil
    bool readString(string, bool warn=True) nogil
    bool init() nogil
    bool next() nogil
    void stat() nogil

    Event event
    ParticleData particleData

cdef class PyPythia:
  cdef Pythia * pythia
  cdef Pythia * get_pythia(self) nogil
  cdef ParticleData * particle_data(self) nogil

cdef class PyParticle:
  cpdef int id
  cpdef int status

  cpdef int mother1
  cpdef int mother2
  cpdef int daughter1
  cpdef int daughter2

  cpdef double px
  cpdef double py
  cpdef double pz
  cpdef double e

  cpdef double xProd
  cpdef double yProd
  cpdef double zProd
  cpdef double tProd

  cpdef double xDec
  cpdef double yDec
  cpdef double zDec
  cpdef double tDec

  cpdef double tau

  cpdef bool isFinal(self)

cimport numpy as cnp
ctypedef cnp.float32_t FLOAT
ctypedef cnp.float32_t float32
ctypedef cnp.float64_t float64
  