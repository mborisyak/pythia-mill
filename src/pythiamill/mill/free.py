from .cached import PythiaMillBase

class FreePythiaMill(PythiaMillBase):
  def __init__(self, options,
               cache_size=None, n_workers=None, log=None, seed=None):

    super(FreePythiaMill, self).__init__(
      detector_factory=None,
      options=options,
      batch_size=None,
      n_workers=n_workers,
      log=log,
      seed=seed
    )

    self.cache_size = (2 * len(self.processes)) if cache_size is None else cache_size

    for _ in range(self.cache_size):
      self.command_queue.put(tuple())

  def __iter__(self):
    return self

  def __next__(self):
    return self._sample()

  def next(self):
    return self._sample()

  def _sample(self):
    if self.processes is None:
      raise ValueError('Mill has already been stopped!')

    _, result = self.queue.get(block=True)
    if isinstance(result, Exception):
      raise result

    self.command_queue.put(tuple())
    return result

  def sample(self, size=None):
    if size is None:
      return self._sample()

    else:
      data = list()
      for _ in range(size):
        batch = self._sample()
        data.append(batch)

      return data

  def __call__(self, size=None):
    return self.sample(size)