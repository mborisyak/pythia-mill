import numpy as np
from .common import PythiaMillBase

__all__ = [
  'CachedPythiaMill'
]

class CachedPythiaMill(PythiaMillBase):
  def __init__(self, detector_factory, options, detector_args=(), batch_size=16,
               cache_size=None, n_workers=None, log=None, seed=None):

    super(CachedPythiaMill, self).__init__(
      detector_factory=detector_factory,
      options=options,
      batch_size=batch_size,
      n_workers=n_workers,
      log=log,
      seed=seed
    )

    self.cache_size = (2 * len(self.processes)) if cache_size is None else cache_size
    self.detector_args = detector_args

    for _ in range(self.cache_size):
      self.command_queue.put(self.detector_args)

  def __iter__(self):
    return self

  def __next__(self):
    return self._sample()

  def next(self):
    return self._sample()

  def _sample(self):
    if self.processes is None:
      raise ValueError('Mill has already been stopped!')

    _, result = self.queue.get(block=True)

    if isinstance(result, Exception):
      raise result

    self.command_queue.put(self.detector_args)
    return result

  def sample(self, size=None):
    if size is None:
      return self._sample()
    else:
      current_size = 0
      data = []
      while current_size < size:
        batch = self._sample()
        data.append(batch)
        current_size += batch.shape[0]

      return np.concatenate(data, axis=0)[:size]

  def __call__(self, size=None):
    return self.sample(size)