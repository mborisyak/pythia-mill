from ..pythia import launch_pythia, detector_pythia_worker, free_pythia_worker

import numpy as np
import multiprocessing as mp
from multiprocessing import current_process

import os
import signal

__all__ = [
  'PythiaMillBase',

  'detector_pythia_blade',
  'free_pythia_blade',
]

def free_pythia_blade(command_queue, queue, options):
  try:
    pythia = launch_pythia(options)

    while True:
      args = command_queue.get(block=True)

      if args is None:
        queue.put(None, block=True)
        break

      try:
        result = free_pythia_worker(pythia)
        queue.put((None, result), block=False)
      except Exception as e:
        queue.put((args, e), block=False)

  except Exception as e:
    queue.put((None, e), block=False)


def detector_pythia_blade(detector_factory, command_queue, queue, options, batch_size=1):
  try:
    detector_instance = detector_factory()

    event_size = detector_instance.event_size()
    buffer = np.ndarray(shape=(batch_size, event_size), dtype='float32')
    pythia = launch_pythia(options)

    while True:
      args = command_queue.get(block=True)

      if args is None:
        queue.put(None, block=True)
        break

      try:
        detector_pythia_worker(detector_instance, pythia, buffer, args)
        queue.put((args, buffer.copy()), block=False)
      except Exception as e:
        queue.put((args, e), block=False)

  except Exception as e:
    queue.put((None, e), block=False)

def pythia_process(detector_factory, command_queue, queue, options, log=None, batch_size=1):
  import sys
  if current_process().name != 'MainProcess':
    pid = os.getpid()
    sys.stdout = open(os.path.join(log, 'log_%d.stdout' % pid), 'w') if log is not None else open(os.devnull, 'w')
    sys.stderr = open(os.path.join(log, 'log_%d.stderr' % pid), 'w') if log is not None else open(os.devnull, 'w')

  if detector_factory is None:
    return free_pythia_blade(command_queue, queue, options)
  else:
    return detector_pythia_blade(detector_factory, command_queue, queue, options, batch_size=batch_size)

class PythiaMillBase(object):
  def __init__(self, detector_factory, options, batch_size=16,
               n_workers=None, log=None, seed=None):

    if seed is not None:
      if any([ 'Random:seed' in option for option in options]):
        import warnings
        warnings.warn('randomize_seed is turned off as Pythia options contain seed settings.')
        seed = None
    else:
      if not any(['Random:seed' in option for option in options]):
        import warnings
        warnings.warn('`seed` argument is not specified and Pythia options does not contain `Random:seed` options. '
                      'This may result in duplicating samples.')

    if n_workers is None:
      cpu_count = os.cpu_count()
      n_workers = max(1, cpu_count - 1) if cpu_count is not None else 1

    if seed is not None:
      import random
      random.seed(seed)
      seeds = set()

      while len(seeds) < n_workers:
        seeds.add(random.randrange(1, 900000000))

      seeds = list(seeds)
      random.shuffle(seeds)
    else:
      seeds = list()

    if log is not None:
      os.makedirs(log, exist_ok=True)

    ctx = mp.get_context('spawn')

    self.command_queue = ctx.Queue()
    self.queue = ctx.Queue()

    self.processes = [
      ctx.Process(
        target=pythia_process,
        kwargs=dict(
          detector_factory=detector_factory,
          command_queue=self.command_queue,
          queue=self.queue,
          log=log,
          options=options if seed is None else (options + ['Random:setSeed=on', 'Random:seed=%d' % seeds[i]]),
          batch_size=batch_size
        ),
        daemon=True
      )
      for i in range(n_workers)
    ]

    for p in self.processes:
      p.start()

  def terminate(self):
    if self.processes is None:
      return

    for p in self.processes:
      try:
        os.kill(p.pid, signal.SIGKILL)
      except Exception as e:
        print('Failed to stop pythia blade (pid %d), reason: %s' % (p.pid, e))

    self.processes = None

  def __del__(self):
    self.terminate()

  def shutdown(self):
    try:
      if self.processes is None:
        return

      for _ in self.processes:
        self.command_queue.put(None)

      stopped = 0
      while True:
        c = self.queue.get(block=True)

        if c is None:
          stopped += 1

        if stopped >= len(self.processes):
          break
    except Exception as e:
      import warnings
      warnings.warn('Failed to stop mill gracefully. Killing brutally.')
      warnings.warn('%s' % e)
    finally:
      self.terminate()
      self.processes = None