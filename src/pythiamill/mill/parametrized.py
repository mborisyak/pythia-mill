import numpy as np

from .common import PythiaMillBase

__all__ = [
  'ParametrizedPythiaMill',
  'sample_parametrized'
]

class ParametrizedPythiaMill(PythiaMillBase):
  def __init__(self, detector_factory, options, batch_size=16, n_workers=None, log=None, seed=None):
    super(ParametrizedPythiaMill, self).__init__(
      detector_factory=detector_factory,
      options=options,
      batch_size=batch_size,
      n_workers=n_workers,
      seed=seed,
      log=log
    )

    self._pending_requests = 0

  def pending_requests(self):
    return self._pending_requests

  def request(self, *args):
    if self.processes is None:
      raise ValueError('Mill has already been stopped!')

    self._pending_requests += 1
    self.command_queue.put(args)


  def retrieve(self):
    if self.processes is None:
      raise ValueError('Mill has already been stopped!')

    if self._pending_requests <= 0:
      raise ValueError('Attempt to retrieve without request! Consider calling `request` method first.')

    try:
      args, result = self.queue.get(block=True)
      self._pending_requests -= 1

      if isinstance(result, Exception):
        raise result

      return args, result
    except:
      import warnings
      warnings.warn('An exception occurred while retrieving. Cleaning queue.')
      while not self.queue.empty():
        self.queue.get()

      raise

  def sample(self, detector_configurations, progress=None):
    return sample_parametrized(self, detector_configurations, progress=progress)

def sample_parametrized(mill: ParametrizedPythiaMill, detector_configurations, progress=None):
  """
  Asynchronously samples from `mill` data given pythia configurations.

  This function should be preferred to a request-retrieve for-loop as the latter
  does not take advantage of parallel execution.

  Note, that returned values does not preserve order of `detector_configurations`.

  :param mill: an instance of ParametrizedMill
  :param detector_configurations: a list of pythia parameters;
  :param progress: if set to `tqdm` retrieval progress is shown.
  :return:
    - parameters: array of shape `<number of samples> x <parameters dim>`, parameters for each sample;
    - samples: array of shape `<number of samples> x 1 x 32 x 32`, sampled events.
  """
  if progress is None:
    progress = lambda x, *args, **kwargs: x

  try:
    ### sending requests to the queue
    for args in detector_configurations:
      mill.request(*args)

    ### retrieving results
    data = [
      mill.retrieve()
      for _ in progress(range(len(detector_configurations)))
    ]

    samples = np.vstack([samples for params, samples in data])
    params = np.vstack([np.array([params] * samples.shape[0], dtype='float32') for params, samples in data])

    return params, samples
  finally:
    while mill.pending_requests() > 0:
      mill.retrieve()